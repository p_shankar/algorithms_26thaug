#include <iostream>
#include <vector>
#include <sstream>

using namespace std;
int split(string str){
  int startIndex = 0;
  int length = 0;
  int hour1 = 0;
  int hour2 = 0;
  int minutes1 = 0;
  int minutes2 = 0;
  bool firstColon = false;
  for(int i=0; i<str.size(); i++){
    if(str[i] == ':'){
      if(firstColon == false){
        firstColon = true;
      }else{
        firstColon = false;
      }
      string temp = str.substr(startIndex, length);
      stringstream oss(temp);
      if(firstColon){
        oss >> hour1;
      }else{
        oss >> hour2;
      }
      startIndex = i+1;
      length = 0;
    }else if(str[i] == 'a' || str[i] == 'p'){
      if(firstColon == true){
        string temp = str.substr(startIndex, length);
        stringstream oss(temp);
        oss >> minutes1;
        startIndex = i+3;
        length = 0;
        if(str[i] == 'p'){
          if(hour1 < 12)
            hour1 += 12;
        }
        if(str[i] == 'a'){
          if(hour1 == 12){
            hour1 = 0;
          }
        }
      }else{
        string temp = str.substr(startIndex, length);
        stringstream oss(temp);
        oss >> minutes2;
        startIndex = i+2;
        length = 0;
        if(str[i] == 'p'){
          if(hour2 < 12)
            hour2 += 12;
        }
        if(str[i] == 'a'){
          if(hour2 == 12)
            hour2 = 0;
        }
      }
    }else{
      length ++;
    }
  }
  int totalminutes1 = hour1*60 + minutes1;
  int totalminutes2 = hour2*60 + minutes2;
  int result = totalminutes2 - totalminutes1;
  if(result < 0){
    result = 1440 + result;
  }
  return result;
}

int CountingMinutesI(string str){
  return split(str);
}

int main(){
  string str = string("1:23am-1:08am");
  cout << CountingMinutesI(str) << endl;
}
