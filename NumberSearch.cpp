#include <iostream>
#include <cmath>
#include <sstream>

using namespace std;

int NumberAddition(string str) { 

  // code goes here
  bool firstNumberFound = false;
  int firstIndex = 0;
  int length = 0;
  int sum = 0;
  int numberOfCharacters = 0;
  
  for(int i=0; i<(int)str.size(); i++){
    if(str[i] > 47 && str[i] < 58){
      if(!firstNumberFound){
        firstNumberFound = true;
        firstIndex = i;
        length = 0;
      }else{
        if(firstNumberFound)
          length ++;
      }
    }else{
      if(firstNumberFound){
        stringstream oss(str.substr(firstIndex, length+1));
        int temp; oss >> temp;
        sum += temp;
        firstNumberFound = false;
      }
      if((str[i] > 64 && str[i] < 91) || (str[i] > 96 && str[i] < 123)){
        numberOfCharacters++;
      }
    }
  }
  if(firstNumberFound){
    stringstream oss(str.substr(firstIndex, length+1));
    int temp; oss >> temp;
    sum += temp;
    firstNumberFound = false;
  }
  cout << sum << endl;
  cout << numberOfCharacters << endl;
  return round(float(sum)/(float)numberOfCharacters);          
}

int main() { 
  
  // keep this function call here
  string str = "Hello6 9World 2, Nic8e D7ay!";
  cout << NumberAddition(str) << endl;
  return 0;
} 
