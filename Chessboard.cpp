#include <iostream>
#include <cstring>
#include <vector>

using namespace std;

class Chessboard{
  private:
    const bool available;
    const int squares, norm;
    bool *column, *leftDiagonal, *rightDiagonal;
    int *positionInRow, howMany;
    void putQueen(int row);
    void printBoard(ostream &);
    void initializeBoard();
  public:
    Chessboard();
    Chessboard(int size);
    int findSolution();
};

Chessboard::Chessboard() : available(true), squares(8), norm(squares-1){
  initializeBoard();
}

Chessboard::Chessboard(int n) : available(true), squares(n), norm(squares-1){
  initializeBoard();
}

void Chessboard::initializeBoard(){
  register int i;
  column = new bool[squares];
  positionInRow = new int[squares];
  leftDiagonal = new bool[squares*2 - 1];
  rightDiagonal = new bool[squares*2 - 1];
  for(i=0; i<squares; i++){
    positionInRow[i] = -1;
  }
  for(i=0; i<squares; i++){
    column[i] = available;
  }
  for(i=0; i<squares*2-1; i++){
  leftDiagonal[i] = rightDiagonal[i] = available;
  }
  howMany = 0;
}

void Chessboard::putQueen(int row){
  for(int col = 0; col < squares; col++){
    if(column[col] == available && leftDiagonal[row+col] == available && rightDiagonal[row-col+norm] == available){
      positionInRow[row] = col;
      column[col] = !available;
      leftDiagonal[row+col] = !available;
      rightDiagonal[row-col+norm] = !available;
    }
  }
}

