#include <iostream>

using namespace std;

int recursiveMultiply(int n1, int n2, int sum=0){
  if(n2 == 0){
    return sum;
  }
  recursiveMultiply(n1, n2-1, sum+n1);
}

int main(){
  int value = recursiveMultiply(13, 4);
  cout<<value<<endl;
}
