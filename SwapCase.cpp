#include <iostream>
#include <sstream>
#include <vector>
#include <string>

using namespace std;

string SwapCase(string str){
  for(int i=0; i<str.size(); i++){
    if(str[i] > 64 && str[i] < 92){
      str[i] += 32;
    }else if(str[i] > 96 && str[i] < 123){
      str[i] -= 32;
    }
  }
  return str;
}

int main(){
  string str = "Hello. World!";
  cout << SwapCase(str);
  return 0;
}
