#include <iostream>
#include <cstring>
#include <sstream>
#include <vector>
#include <utility>
#include <map>
#include <stack>
#include <vector>
#include <climits>

using namespace std;

void split(const vector<string> stringArr, map<string, int> &nodeNameKey, map<int, string> &nameNodeKey, const int numNodes){
  for(int i=1; i<numNodes+1; i++){
    nodeNameKey.insert(make_pair(stringArr[i], i-1));
    nameNodeKey.insert(make_pair(i-1, stringArr[i]));
  }
}

class Edge{
  public:
    string node1;
    string node2;
    int weight;
};

Edge splitIntoNodes(string input){
  int positionOfColon1 = 0;
  int positionOfColon2 = 0;
  for(int i=0; i < (int)input.size(); i++){
    if(input[i] == '|'){
      if(!positionOfColon1)
        positionOfColon1 = i;
      else
        positionOfColon2 = i;
    }
  }
  Edge os;
  os.node1 = input.substr(0, positionOfColon1);
  os.node2 = input.substr(positionOfColon1+1, positionOfColon2-positionOfColon1-1);
  string str = input.substr(positionOfColon2+1, input.size()-positionOfColon2-1);
  stringstream oss(str);
  oss >> os.weight;
  return os;
}

class WeightedPathNode{
  public:
    int distance;
    string prevNodeName;
    int prevIndex;
};

string WeightedPath(vector<string> stringArr){
  stringstream oss(stringArr[0]);
  int numNodes;
  oss >> numNodes;
  map<string, int> nodeNameKey;
  map<int, string> nameNodeKey;
  split(stringArr, nodeNameKey, nameNodeKey, numNodes);
  vector< vector<int> > adjacencyMatrix;
  adjacencyMatrix.resize(numNodes);
  for(int i=0; i<numNodes; i++){
    adjacencyMatrix[i].resize(numNodes);
  }
  for(int i=0; i<numNodes; i++){
    for(int j=0; j<numNodes; j++){
      adjacencyMatrix[i][j] = 0;
    }
  }
  for(int i=numNodes+1; i< (int)stringArr.size(); i++){
    Edge twoNodes = splitIntoNodes(stringArr[i]);
    int index1 = nodeNameKey[twoNodes.node1];
    int index2 = nodeNameKey[twoNodes.node2];
    adjacencyMatrix[index1][index2] = twoNodes.weight;
    adjacencyMatrix[index2][index1] = twoNodes.weight;
  }
  
  //find the shortest path now.
  vector<int> completed;
  vector<int> queue;
  queue.push_back(0);
  vector<WeightedPathNode> fromStartNode;
  for(int i=0; i<numNodes; i++){
    WeightedPathNode temp;
    temp.prevIndex = i;
    temp.prevNodeName = nameNodeKey[i];
    temp.distance = INT_MAX;
    fromStartNode.push_back(temp);
  }
  fromStartNode[0].distance = 0;
  while(((int)completed.size() != numNodes) && (queue.size() > 0)){
    int currentNode = queue.front(); 
    queue.erase(queue.begin());
    for(int i=0; i < numNodes; i++){
      if(adjacencyMatrix[currentNode][i] > 0){
        bool isCompleted = false;
        for(int j=0; j < (int)completed.size(); j++){
          if(completed[j] == i){
            isCompleted = true;
          }
        }
        if(!isCompleted)
          queue.push_back(i);
        int distance = fromStartNode[currentNode].distance + adjacencyMatrix[currentNode][i];
        if(distance < fromStartNode[i].distance){
          fromStartNode[i].distance = distance;
          fromStartNode[i].prevIndex = currentNode;
          fromStartNode[i].prevNodeName = nameNodeKey[currentNode];
        }
      }
    }
    completed.push_back(currentNode);
  }
  if(fromStartNode[numNodes-1].distance == INT_MAX)
    return string("-1");
  else{
    int i = numNodes-1;
    string str = string(nameNodeKey[i]);
    while(fromStartNode[i].prevIndex != i){
      str = fromStartNode[i].prevNodeName + string("-") + str;
      i = fromStartNode[i].prevIndex;
    }
    return str;
  }
}

/* There is a problem with this code.. the queue is not sorted to take the shorted path to the next iteration.
 * This will result in erroneous results in which a path with the higher weight might get sucked into completed
 * set before going through paths with smaller weights.
 *
 * Will fix it at a later time.. right now will be doing something else.
 */

int main(){
  string inputI[] = {"6","A","B","C","D","E","F","B|A|2","A|F|12","A|C|4","B|D|1","D|E|1","C|D|4","F|E|1"};
  vector<string> input(inputI, inputI+sizeof(inputI)/sizeof(inputI[0]));
  cout << WeightedPath(input) << endl;
}
