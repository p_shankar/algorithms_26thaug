#include <iostream>

using namespace std;

void putCommas(int n, int i=0){
  if(n/1000 != 0){
    i=i+1;
    putCommas(n/1000, i);
  }
  if(i == 1){
    cout<<n%1000<<"."<<endl;
  }else{
    cout<<n%1000<<",";
  }
}

int main(){
  putCommas(1234567);
}
