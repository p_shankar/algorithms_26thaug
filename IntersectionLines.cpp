#include <iostream>
#include <cstring>
#include <string>
#include <sstream>
#include <vector>
#include <cmath>
#include <climits>

using namespace std;

class Point{
  public:
    double x;
    double y;

    Point(double _x, double _y){
      x = _x;
      y = _y;
    }

    Point(string _x, string _y){
      stringstream oss1(_x);
      stringstream oss2(_y);
      oss1 >> x;
      oss2 >> y;
    }

    static long long gcd( long long a, long long b){
      if( a == b){
        return a;
      }else if( a == 1){
        return 1;
      }else if( b == 1){
        return 1;
      }else if ( a > b ){
        return gcd( a - b, b );
      }else if( a < b){
        return gcd( b - a, a);
      }
      return 1;
    }

    string getStringRepresentation(double val){
      val = floor(val*100+0.5)/100;
      long long integerVal = val * 1000000;
      bool negative = false;
      if(integerVal < 0){
        integerVal = integerVal*-1;
        negative = true;
      }
      long long gcdVal = gcd( integerVal, 1000000);
      long long numerator  = integerVal / gcdVal;
      long long denominator = 1000000 / gcdVal;
      stringstream oss;
      if(numerator == 0){
        oss << 0;
        return oss.str();
      }
      if(!negative){
        if(denominator != 1)
          oss << numerator << string("/") << denominator;
        else
          oss << numerator;
      }else{
        if(denominator != 1){
          oss << string("-") << numerator << string("/") << denominator;
        }else 
          oss << string("-") << numerator;
      }

      return oss.str();
    }

    string getPointAsString(){
      stringstream oss;
      string p1 = getStringRepresentation(x);
      string p2 = getStringRepresentation(y);
      oss << string("(") << p1 << string(",") << p2 << string(")");
      return oss.str();
    }
};

class LineEquation{
  public:
    double m;
    double y_intercept;

    LineEquation(Point p1, Point p2){
      if(p1.x - p2.x != 0)
        m = (p1.y - p2.y)/(p1.x - p2.x);
      else 
        m = INT_MAX;
      y_intercept = p2.y - m*p2.x;
    }
};

vector<Point> split(vector<string> strArr){
  vector<Point> points;
  for(int i=0; i < (int)strArr.size(); i++){
    string temp = strArr[i];
    temp.erase(temp.begin());
    temp.erase(temp.size() - 1, 1);
    int indexOfComma = 0;
    for(int j=0; j < (int)temp.size(); j++){
      if(temp[j] == ','){
        indexOfComma = j;
        break;
      }
    }
    string point1 = temp.substr(0, indexOfComma);
    string point2 = temp.substr(indexOfComma+1, temp.size()-indexOfComma-1);
    points.push_back(Point(point1, point2));
  }

  return points;
}

string IntersectingLines(vector<string> strArr){
  vector<Point> points = split(strArr);  
  LineEquation eq1(points[0], points[1]);
  LineEquation eq2(points[2], points[3]);

  if(abs(eq1.m - eq2.m) < 1e-6){
    return "no intersection";
  }else{
    double x_intersect = (eq1.y_intercept - eq2.y_intercept)/(eq2.m - eq1.m);
    double y_intersect = eq1.m * x_intersect + eq1.y_intercept;
    Point intersectionPoint(x_intersect, y_intersect);
    return intersectionPoint.getPointAsString();
  }
}

int main(){
  string strArray[] = {"(0,0)","(0,-1)","(1,1)","(0,1)"};
  vector<string> strArr(strArray, strArray+sizeof(strArray)/sizeof(strArray[0]));
  cout << IntersectingLines(strArr);
  return 0;
}
