#include <iostream>
#include <sstream>
#include <string>
#include <algorithm>

using namespace std;

string SwapII(string str){
  bool firstNumFound = false;
  int firstNumIndex = 0;
  int length = 0;
  for(int i=0; i < (int)str.size(); i++){
    if(str[i] > 64 && str[i] < 91){
      str[i] += 32;
      if(firstNumFound){
        length++;
      }
    }else if(str[i] > 96 && str[i] < 123){
      str[i] -= 32;
      if(firstNumFound){
        length++;
      }
    }else if(str[i] > 47 && str[i] < 58){
      if(!firstNumFound){
        firstNumFound = true;
        firstNumIndex = i;
        length = 0;
      }else{
	  if(length == 0)
	    continue;
          swap(str[firstNumIndex], str[firstNumIndex + length + 1]);
          firstNumFound = false;
      }
    }else{
      if(firstNumFound)
        firstNumFound = false;
    }
  }
  return str;
}

int main(){
  string str = "2S 6 du5d4e";
  cout << SwapII(str) << endl;
  return 0;
}
