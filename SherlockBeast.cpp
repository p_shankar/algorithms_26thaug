#include <iostream>
#include <string>
#include <vector>
#include <sstream>

using namespace std;

class DecentNumber{
  private:
    int N;

  public:
    DecentNumber(int NN){
      N = NN;
    }

    string returnLargestDecentNumber(){
      int x = 0, y = 0;

      for(int i=0; i<=N/5; i++){
        for(int j=0; j<=N/3; j++){
          if(5*i+3*j == N){
            if(j > x){
              x = j;
              y = i;
            }else if(j == 0 && i > y){
              y = i;
            }
          }
        }
      }

      stringstream oss;
      if(x == 0 && y == 0){
        return "-1";
      }

      for(int i=0; i<3*x; i++){
        oss << '5';
      }

      for(int i=0; i<5*y; i++){
        oss << '3';
      }

      return oss.str();
    }
};

int main(){
  int T;
  cin >> T;

  int *N = new int[T];

  for(int i=0; i<T; i++){
    cin >> N[i];
  }

  for(int i=0; i<T; i++){
    DecentNumber d(N[i]);
    cout << d.returnLargestDecentNumber() << endl;
  }
}
