#include <iostream>
#include <vector>
#include <string>

using namespace std;

class RedJohn{
  private:
    int N;

  public:
    RedJohn(int NN){
      N = NN;
    }

    int getConfiguration(int n){
      int *f = new int[n+1];
      for(int i=0; i<n+1; i++){
        f[i] = 0;
      }
      f[0] = 1;
      f[1] = 1;
      for(int i=2; i<n+1; i++){
        if(i-4 >= 0){
          f[i] = f[i-1] + f[i-4];
        }else{
          f[i] = 1;
        }
      }

      return f[n];
    }
};

int main(int argc, char *argv[]){
  int N = 7;
  RedJohn rj(N);
  cout << rj.getConfiguration(N) << endl;
}
