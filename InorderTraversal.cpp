#include <iostream>
#include <algorithm>
#include <vector>
 
using namespace std;
 
template <class T>
class Node{
  public:
    T data;
    Node *left;
    Node *right;
};
 
template <class T>
class Tree{
  private:
    Node<T> *Root;
 
  public:
    Tree<T>(vector<string> inorder, vector<string> order, bool preOrPost){
      Root = constructTree(inorder, order, preOrPost);
    }
 
    vector<T> splitleft(vector<T> inorder, T rootName){
      vector<T> temp;
      for(int i=0; i < (int)inorder.size(); i++){
        if(inorder[i] != rootName){
          temp.push_back(inorder[i]);
        }else{
          break;
        }
      }
      return temp;
    }
 
    vector<T> splitright(vector<T> inorder, T rootName){
      int indexOfRoot = 0;
      vector<T> temp;
      for(int i=0; i < (int)inorder.size(); i++){
        if(inorder[i] == rootName){
          indexOfRoot = i+1;
          break;
        }
      }
      while(indexOfRoot < (int)inorder.size()){
        temp.push_back(inorder[indexOfRoot++]);
      }
      return temp;
    }
    
    Node<T>* constructTree(vector<T> inorder, vector<T> &order, bool preOrPost){
      //create a new node 
      Node<T> *root = 0;
      if(!preOrPost){
        if(order.size() > 0){
          root = new Node<T>();
          root->data = order[0];
          order.erase(order.begin());
          //split the inorder array into two parts right and left of the order index;
          vector<T> inOrder1 = splitleft(inorder, root->data);
          vector<T> inOrder2 = splitright(inorder, root->data);
          if(inOrder1.size() > 0){
            root->left = constructTree(inOrder1, order, preOrPost);
          }else{
            root->left = 0;
          }
          if(inOrder2.size() > 0){
            root->right = constructTree(inOrder2, order, preOrPost); 
          }else
            root->right = 0;
          return root;
        }else{
          return root;
        }
      }else{
        if(order.size() > 0){
          root = new Node<T>();
          root->data = order[order.size()-1];
          order.erase(order.end());
          //split the inorder array into two parts right and left of the order index;
          vector<T> inOrder1 = splitleft(inorder, root->data);
          vector<T> inOrder2 = splitright(inorder, root->data);
          if(inOrder2.size() > 0){
            root->right = constructTree(inOrder2, order, preOrPost); 
          }else
            root->right = 0;
          if(inOrder1.size() > 0){
            root->left = constructTree(inOrder1, order, preOrPost);
          }else{
            root->left = 0;
          }
          return root;
        }else{
          return root;
        }
      }
    }
    
    bool testPresence(vector<Node<T> *> visited, Node<T> *testNode){
      for(int i=0; i < (int)visited.size(); i++){
        if(visited[i]->data == testNode->data){
          return true;
        }
      }
      return false;
    }
 
    void printTree(){
      vector<Node<T> *> stack;
      stack.push_back(Root);
      vector<Node<T> *> visited;
      while(!stack.empty()){
        cout << stack.back()->data << endl;
        Node<T> *temp = stack.back();
        visited.push_back(stack.back());
        stack.pop_back();
        if(temp->left != 0){
          bool found = testPresence(visited, temp->left);
          if(found){
            continue;
          }
          stack.push_back(temp->left);
        }
        if(temp->right != 0){
          bool found = testPresence(visited, temp->right);
          if(found){
            continue;
          }
          if(found){
            continue;
          }
          stack.push_back(temp->right);
        }
      }
    }
};

int main(){
  string preorderI[] = {"F", "B", "A", "D", "C", "E", "G", "I", "H"};
  string inorderI[] = {"A", "B", "C", "D", "E", "F", "G", "H", "I"};
  string postorderI[] = {"A", "C", "E", "D", "B", "H", "I", "G", "F"};
  vector<string> preorder(preorderI, preorderI+sizeof(preorderI)/sizeof(preorderI[0]));
  vector<string> inorder(inorderI, inorderI+sizeof(inorderI)/sizeof(inorderI[0]));
  vector<string> postorder(postorderI, postorderI+sizeof(postorderI)/sizeof(postorderI[0]));
  bool preOrPost = false; //preOrPost = false for Preorder preOrPost = true for PostOrder
  Tree<string> tree1(inorder, preorder, preOrPost);
  tree1.printTree();
  preOrPost = true;
  Tree<string> tree2(inorder, postorder, preOrPost);
  tree2.printTree();
  return 0;
}
