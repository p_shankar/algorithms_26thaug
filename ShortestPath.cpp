#include <iostream>
#include <cstring>
#include <sstream>
#include <vector>
#include <utility>
#include <map>
#include <stack>
#include <vector>
#include <climits>

using namespace std;

void split(const vector<string> stringArr, map<string, int> &nodeNameKey, map<int, string> &nameNodeKey, const int numNodes){
  for(int i=1; i<numNodes+1; i++){
    nodeNameKey.insert(make_pair(stringArr[i], i-1));
    nameNodeKey.insert(make_pair(i-1, stringArr[i]));
  }
}

pair<string, string> splitIntoNodes(string input){
  int positionOfColon = 0;
  for(int i=0; i < (int)input.size(); i++){
    if(input[i] == '-'){
      positionOfColon = i;
      break;
    }
  }
  pair<string, string> os;
  os.first = input.substr(0, positionOfColon);
  os.second = input.substr(positionOfColon+1, input.size()-positionOfColon-1);
  return os;
}

class ShortestPathNode{
  public:
    int distance;
    string prevNodeName;
    int prevIndex;
};

string ShortestPath(vector<string> stringArr){
  stringstream oss(stringArr[0]);
  int numNodes;
  oss >> numNodes;
  map<string, int> nodeNameKey;
  map<int, string> nameNodeKey;
  split(stringArr, nodeNameKey, nameNodeKey, numNodes);
  vector< vector<int> > adjacencyMatrix;
  adjacencyMatrix.resize(numNodes);
  for(int i=0; i<numNodes; i++){
    adjacencyMatrix[i].resize(numNodes);
  }
  for(int i=0; i<numNodes; i++){
    for(int j=0; j<numNodes; j++){
      adjacencyMatrix[i][j] = 0;
    }
  }
  for(int i=numNodes+1; i< (int)stringArr.size(); i++){
    pair<string, string> twoNodes = splitIntoNodes(stringArr[i]);
    int index1 = nodeNameKey[twoNodes.first];
    int index2 = nodeNameKey[twoNodes.second];
    adjacencyMatrix[index1][index2] = 1;
    adjacencyMatrix[index2][index1] = 1;
  }
  
  //find the shortest path now.
  vector<int> completed;
  vector<int> queue;
  queue.push_back(0);
  vector<ShortestPathNode> fromStartNode;
  for(int i=0; i<numNodes; i++){
    ShortestPathNode temp;
    temp.prevIndex = i;
    temp.prevNodeName = nameNodeKey[i];
    temp.distance = INT_MAX;
    fromStartNode.push_back(temp);
  }
  fromStartNode[0].distance = 0;
  while(((int)completed.size() != numNodes) && (queue.size() > 0)){
    int currentNode = queue.front(); 
    queue.erase(queue.begin());
    for(int i=0; i < numNodes; i++){
      if(adjacencyMatrix[currentNode][i] == 1){
        bool isCompleted = false;
        for(int j=0; j < (int)completed.size(); j++){
          if(completed[j] == i){
            isCompleted = true;
          }
        }
        if(!isCompleted)
          queue.push_back(i);
        int distance = fromStartNode[currentNode].distance + 1;
        if(distance < fromStartNode[i].distance){
          fromStartNode[i].distance = distance;
          fromStartNode[i].prevIndex = currentNode;
          fromStartNode[i].prevNodeName = nameNodeKey[currentNode];
        }
      }
    }
    completed.push_back(currentNode);
  }
  if(fromStartNode[numNodes-1].distance == INT_MAX)
    return string("-1");
  else{
    int i = numNodes-1;
    string str = string(nameNodeKey[i]);
    while(fromStartNode[i].prevIndex != i){
      str = fromStartNode[i].prevNodeName + string("-") + str;
      i = fromStartNode[i].prevIndex;
    }
    return str;
  }
}

int main(){
  string inputI[] = {"4","X","Y","Z","W","X-Y","Y-Z","X-W"};
  vector<string> input(inputI, inputI+sizeof(inputI)/sizeof(inputI[0]));
  cout << ShortestPath(input) << endl;
}
