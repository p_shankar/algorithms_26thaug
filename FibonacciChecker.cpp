#include <iostream>
#include <string>
#include <sstream>
#include <vector>

using namespace std;

string FibonacciChecker(int num){
  int fib1 = 0;
  int fib2 = 1;
  int fib;
  bool notfound = true;
  while(notfound){
    if(num == fib1){
      return string("yes");
    }else if(num == fib2){
      return string("yes");
    }else{
      fib = fib1 + fib2;
      if(fib == num)
        return string("yes");
      if(fib > num){
        notfound = false;
      }
      fib1 = fib2;
      fib2 = fib;
    }
  }
  return string("no");
}

int main(){
  int num = 0;
  cout << FibonacciChecker(num) << endl;
}
