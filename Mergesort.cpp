#include <iostream>
#include <vector>

using namespace std;

vector<int> merge(vector<int> array1, vector<int> array2);

void mergesort(vector<int> &array){
  if(array.size() == 2){
    if(array[0] > array[1]){
      int temp = array[0];
      array[0] = array[1];
      array[1] = temp;
    }
    return;
  }

  if(array.size() == 1){
    //do nothing to the array
    return;
  }
  
  vector<int> array1;
  vector<int> array2;
  array1.resize(array.size()/2);
  array2.resize(array.size()-array1.size());

  for(int i=0; i<(int)array.size()/2; i++){
    array1[i] = array[i];
  }

  for(int i=array.size()/2; i<(int)array.size(); i++){
    array2[i-array.size()/2] = array[i];
  }

  mergesort(array1);
  mergesort(array2);
  array = merge(array1, array2);
}

vector<int> merge(vector<int> array1, vector<int> array2){
  int i=0; int j=0;
  vector<int> array;
  while(i < (int)array1.size() && j < (int)array2.size()){
    if(array1[i] < array2[j]){
      array.push_back(array1[i]);
      i++;
    }else if(array1[i] == array2[j]){
      array.push_back(array1[i]);
      array.push_back(array2[j]);
      i++; j++;
    }else{
      array.push_back(array2[j]);
      j++;
    }
  }

  if(i == (int)array1.size()){
    while(j < (int) array2.size())
      array.push_back(array2[j++]);
  }

  if(j == (int)array2.size()){
    while(i < (int)array1.size())
      array.push_back(array1[i++]);
  }
  return array;
}

int main(){
  int a[] = {10,9,8,7,6,5,4,3,2,1};
  vector<int> array(a, a+sizeof(a)/sizeof(a[0]));
  mergesort(array);
  for(int i=0; i<(int)array.size(); i++){
    cout << array[i] << endl;
  }
  return 0;
}
