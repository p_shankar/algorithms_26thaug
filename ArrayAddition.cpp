#include <iostream>
#include <vector>
#include <sstream>
#include <algorithm>

using namespace std;

string ArrayAdditionI(vector<int> arr){
  sort(arr.begin(), arr.end());
  while(arr[0] < 0){
    arr[arr.size()-1] += arr[0];
    arr.erase(arr.begin());
    sort(arr.begin(), arr.end());
  }
  int MaxSum = arr[arr.size()-1];
  int M[MaxSum+1][arr.size()-1];
  for(int i=0; i<MaxSum+1; i++){
    for(int j=0; j<(int)arr.size()-1; j++){
      M[i][j] = 0;
    }
  }
  for(int i=0; i<MaxSum+1; i++){
    if(arr[0] <= i)
      M[i][0] = arr[0];
  }
  
  for(int j=0; j<(int)arr.size()-1; j++){
    M[0][j] = 0;
  }

  for(int i=1; i<MaxSum+1; i++){
    //M[0] = 0, M[1] = (either 1 is there in the array or M
    //check for all the numbers in the array less than i
    for(int j=1; j<(int)arr.size()-1; j++){
      if(i-arr[j] > -1){
        M[i][j] = max(M[i][j-1], M[i-arr[j]][j-1]+arr[j]);
      }else
        M[i][j] = M[i][j-1];
    }
  }

  for(int i=0; i<MaxSum+1; i++){
    for(int j=0; j<(int)arr.size()-1; j++){
      cout << M[i][j] << " ";
    }
    cout << endl;
  }

  //Now this array should be filled 
  //We have to look up the array in reverse to backtrace the path to see which elements were included.
  //Now just test for the first value
  //whether it should be included in nor not
  //find the sum of the included elements
  //see if they are equal to the MaxSum
  //if it is true then return true;
  if(M[MaxSum][arr.size()-2] == MaxSum){
    return "true";
  }
  return "false";
}

int main() { 
   
  // keep this function call here
  /* Note: In C++ you first have to initialize an array and set 
     it equal to the stdin to test your code with arrays. 
     To see how to enter arrays as arguments in C++ scroll down */
     
  int B[] = {3, 5, -1, 8, 13};
  vector<int> A(B, B+sizeof(B)/sizeof(B[0]));
  cout << ArrayAdditionI(A);
  return 0;    
}
